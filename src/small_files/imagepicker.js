var MIImagePicker = {
    create: function(canvasDomObject, fileInputObject, options) {
        return new _MIImagePicker(canvasDomObject, fileInputObject, options);
    },

};

function _MIImagePicker(canvasDomObject, fileInputObject, options) {
    // 检测 Canvas 元素， FileInput 是否存在
    if (!canvasDomObject || !fileInputObject) {
        throw "Can not find the canvasDomObject or the fileInputObject";
        return;
    }
    this.image = new Image(); // 创建一个 Image 对象
    this.image.onload = this.drawImage; // 定义 Image 对象的 src 属性

    this.canvas = canvasDomObject;
    this.canvas.setAttribute('data-resizing', 'N');

    this.file = fileInputObject;
    var defaultOptions = {
        defaultSelectionWidth: 100, // 选取框默认宽度 200px
        defaultSelectionHeight: 100, // 选取框默认高度 200px
        keepAspectRatio: true,
        maxOutputWidth: 600,
        maxOutputHeight: 600,
        minOutputWidth: 100, // 选取框最小宽度 100px
        minOutputHeight: 100, // 选取框最小高度 100px
        onerror: function(code, msg, f) {
            console.log('Error occured ' + code + ' : ' + msg);
            console.log(f);
        }
    };

    // 检测是否有自定义选项
    if (options) {
        this.options = {};
        for (var k in defaultOptions) {
            this.options[k] = options[k] || defaultOptions[k];
        }
    } else {
        this.options = defaultOptions;
    }

    this.selectionX = 0;
    this.selectionY = 0;
    this.selectionHeight = this.options.defaultSelectionHeight; // 加载选项中的设置 默认高度 200px
    this.selectionWidth = this.options.defaultSelectionWidth; // 加载选项中的设置 默认宽度 200px
    this.selectionMinX = 0;
    this.selectionMinY = 0;
    this.selectionMaxX = 0;
    this.selectionMaxY = 0;
    this.handlerWidth = 11;

    // console.log(this.options);
    var self = this;
    this.file.addEventListener('change', function(evt) {
        self.onFileValueChange(evt);
    });
    document.addEventListener('mousemove', function(evt) {self.onMouseMove(evt);}, true); // 绑定鼠标移动事件
    document.addEventListener('mousedown', function(evt) {self.onMouseDown(evt);}, true); // 绑定鼠标按下事件
    document.addEventListener('mouseup', function(evt) {self.onMouseUp(evt);}, true); // 绑定鼠标抬起事件
}

_MIImagePicker.prototype.onMouseMove = function(evt) {
    if (evt.target.id !== this.canvas.id || !this.handlers) return;
    evt.preventDefault(); // 取消事件的默认行为
    if (this.status === -1) {
        this.prepareStatus = -1;
        var rect = this.canvas.getBoundingClientRect(); // 获取 Canvas 元素的大小及其相对于视口的位置
        for (var i = 0, n = this.handlers.length; i < n; i++) {
            if (this.handlers[i].x < 0) continue;
            if (this.handlers[i].x + rect.left < evt.clientX
                && evt.clientX < this.handlers[i].x + rect.left + this.handlers[i].w
                && this.handlers[i].y + rect.top < evt.clientY
                && evt.clientY < this.handlers[i].y + rect.top + this.handlers[i].h) {
                this.prepareStatus = i;
                this.canvas.style.cursor = this.handlers[i].cursor;
                // console.log('handler ' + i + ' hitted ');
                break;
            }
        }
        if (this.prepareStatus === -1) {
            this.canvas.style.cursor = 'default';
        }
    } else {
        var rect = this.canvas.getBoundingClientRect();
        if (evt.clientX < rect.left + this.selectionMinX
            || evt.clientY < rect.top + this.selectionMinY
            || evt.clientX > rect.left + this.selectionMinX + this.selectionMaxX
            || evt.clientY > rect.top + this.selectionMinY + this.selectionMaxY) return;

        var dx = evt.clientX - this.lastX;
        var dy = evt.clientY - this.lastY;
        this.lastX = evt.clientX;
        this.lastY = evt.clientY;
        var x = evt.clientX - rect.left;
        var y = evt.clientY - rect.top;
        switch (this.status) {
            case 0:
                var startRight = this.handlers[4].x + this.handlerWidth / 2;
                var startBottom = this.handlers[4].y + this.handlerWidth / 2;
                var dmax = Math.min(startRight - this.selectionMinX, startBottom - this.selectionMinY);
                var d = Math.min(startRight - x, startBottom - y);
                if (d < this.options.minOutputWidth) d = this.options.minOutputWidth;
                if (d > dmax) d = dmax;
                this.selectionX = startRight - d;
                this.selectionY = startBottom - d;
                this.selectionWidth = d;
                this.selectionHeight = d;
                break;
            case 2:
                var startLeft = this.handlers[6].x + this.handlerWidth / 2;
                var startBottom = this.handlers[6].y + this.handlerWidth / 2;
                var dmax = Math.min(this.selectionMaxX - startLeft, startBottom - this.selectionMinY);
                var d = Math.min(x - startLeft, startBottom - y);
                if (d < this.options.minOutputWidth) d = this.options.minOutputWidth;
                if (d > dmax) d = dmax;
                this.selectionX = startLeft;
                this.selectionY = startBottom - d;
                this.selectionWidth = d;
                this.selectionHeight = d;
                break;
            case 4:
                var startLeft = this.handlers[0].x + this.handlerWidth / 2;
                var startTop = this.handlers[0].y + this.handlerWidth / 2;
                var dmax = Math.min(this.selectionMaxX - startLeft, this.selectionMaxY - startTop);
                var d = Math.min(x - startLeft, y - startTop);
                if (d < this.options.minOutputWidth) d = this.options.minOutputWidth;
                if (d > dmax) d = dmax;
                this.selectionWidth = d;
                this.selectionHeight = d;
                break;
            case 6:
                var startRight = this.handlers[2].x + this.handlerWidth / 2;
                var startTop = this.handlers[2].y + this.handlerWidth / 2;
                var dmax = Math.min(startRight - this.selectionMinX, this.selectionMaxY - startTop);
                var d = Math.min(startRight - x, y - startTop);
                if (d < this.options.minOutputWidth) d = this.options.minOutputWidth;
                if (d > dmax) d = dmax;
                this.selectionY = startTop;
                this.selectionX = startRight - d;
                this.selectionWidth = d;
                this.selectionHeight = d;
                break;
            case 8:
                this.selectionX += dx;
                this.selectionY += dy;
                if (this.selectionX < this.selectionMinX) this.selectionX = this.selectionMinX;
                if (this.selectionY < this.selectionMinY) this.selectionY = this.selectionMinY;
                if (this.selectionX + this.selectionWidth > this.selectionMaxX) this.selectionX = this.selectionMaxX - this.selectionWidth;
                if (this.selectionY + this.selectionHeight > this.selectionMaxY) this.selectionY = this.selectionMaxY - this.selectionHeight;
        }

        // if (this.selectionX < this.selectionMinX) this.selectionX = this.selectionMinX;
        // if (this.selectionY < this.selectionMinY) this.selectionY = this.selectionMinY;
        // if (this.selectionWidth < this.options.minOutputWidth) this.selectionWidth = this.options.minOutputWidth;
        // if (this.selectionHeight < this.options.minOutputHeight) this.selectionHeight = this.options.minOutputHeight;
        // if (this.selectionX + this.selectionWidth > this.selectionMaxX) this.selectionWidth = this.selectionMaxX - this.selectionX;
        // if (this.selectionY + this.selectionHeight > this.selectionMaxY) this.selectionHeight = this.selectionMaxY - this.selectionY;
        this.drawBackground();
        this.drawImage();
        this.drawSelection();
    }
};

_MIImagePicker.prototype.onMouseDown = function(evt) {
    if (evt.target.id !== this.canvas.id || !this.handlers || this.prepareStatus === -1) return;
    this.lastX = evt.clientX;
    this.lastY = evt.clientY;
    this.status = this.prepareStatus;
    // console.log(this.selectionX, this.selectionY, this.selectionWidth, this.selectionHeight);
};

_MIImagePicker.prototype.onMouseUp = function(evt) {
    this.prepareStatus = -1;
    this.status = -1;
};


_MIImagePicker.prototype.drawBackground = function() {
    var ctx = this.canvas.getContext('2d');
    var cw = + this.canvas.getAttribute('width');
    var ch = + this.canvas.getAttribute('height');

    ctx.fillStyle = '#666';
    ctx.fillRect(0, 0, cw, ch);
};

_MIImagePicker.prototype.drawImage = function() {
    var ctx = this.canvas.getContext('2d');
    var cw = + this.canvas.getAttribute('width');
    var ch = + this.canvas.getAttribute('height');
    var iw = this.image.width;
    var ih = this.image.height;
    var x = 0;
    var y = 0;
    var dw = 0;
    var dh = 0;
    if (iw < cw && ih < ch) {
        x = (cw - iw) / 2;
        y = (ch - ih) / 2;
        dw = iw;
        dh = ih;
    } else {
        var factor = Math.max(iw / cw, ih / ch);
        dw = iw / factor;
        dh = ih / factor;
        x = (cw - dw) / 2;
        y = (ch - dh) / 2;
    }

    x = Math.round(x);
    y = Math.round(y);
    dw = Math.round(dw);
    dh = Math.round(dh);

    ctx.drawImage(this.image, x, y, dw, dh);

    this.selectionMinX = x;
    this.selectionMinY = y;
    this.selectionMaxX = x + dw;
    this.selectionMaxY = y + dh;
};

_MIImagePicker.prototype.onImageReady = function() {
    this.drawBackground();
    this.drawImage();
    var dw = this.selectionMaxX - this.selectionMinX;
    var dh = this.selectionMaxY - this.selectionMinY;
    var factor = 1.0;
    if (this.selectionWidth < dw && this.selectionHeight < dh) {
    } else {
        var factor = Math.max(this.selectionWidth / dw, this.selectionHeight / dh);
        this.selectionWidth /= factor;
        this.selectionHeight /= factor;
    }
    this.selectionX = this.selectionMinX + (dw - this.selectionWidth) / 2;
    this.selectionY = this.selectionMinY + (dh - this.selectionHeight) / 2;

    this.drawSelection();
};

_MIImagePicker.prototype.drawSelection = function() {
    var ctx = this.canvas.getContext('2d');
    ctx.save();
    //ctx.globalAlpha = 0.7;
    ctx.lineWidth = 1;
    ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';

    // 使用非零环绕规则绘制一个中空的矩形
    // 从左上角开始，外框顺时针方向，内框逆时针方向。
    ctx.beginPath();
    ctx.moveTo(this.selectionMinX, this.selectionMinY);
    ctx.lineTo(this.selectionMaxX, this.selectionMinY);
    ctx.lineTo(this.selectionMaxX, this.selectionMaxY);
    ctx.lineTo(this.selectionMinX, this.selectionMaxY);
    ctx.lineTo(this.selectionMinX, this.selectionMinY);
    ctx.lineTo(this.selectionX, this.selectionY);
    ctx.lineTo(this.selectionX, this.selectionY + this.selectionHeight);
    ctx.lineTo(this.selectionX + this.selectionWidth, this.selectionY + this.selectionHeight);
    ctx.lineTo(this.selectionX + this.selectionWidth, this.selectionY);
    ctx.lineTo(this.selectionX, this.selectionY);
    ctx.fill();

    // the resize handlers
    if (!this.handlers) {
        this.handlers = [];
    }
    var handlerD = this.handlerWidth / 2
    // top-left - 0, top-middle - 1, top-right - 2 ....
    this.handlers[0] = {x: this.selectionX - handlerD, y: this.selectionY - handlerD, cursor: 'nw-resize'};
    if (!this.keepAspectRatio)
        this.handlers[1] = {x: -1, y: -1};
    else
        this.handlers[1] = {x: this.selectionX + this.selectionWidth / 2 - handlerD, y: this.selectionY - handlerD, cursor: 'n-resize'};

    this.handlers[2] = {x: this.selectionX + this.selectionWidth - handlerD, y: this.selectionY - handlerD, cursor: 'ne-resize'};
    if (!this.keepAspectRatio)
        this.handlers[3] = {x: -1, y: -1};
    else
        this.handlers[3] = {x: this.selectionX + this.selectionWidth - handlerD, y: this.selectionY + this.selectionHeight / 2 - handlerD, cursor: 'e-resize'};

    this.handlers[4] = {x:  this.selectionX + this.selectionWidth - handlerD, y: this.selectionY + this.selectionHeight - handlerD, cursor: 'se-resize'};
    if (!this.keepAspectRatio)
        this.handlers[5] = {x: -1, y: -1};
    else
        this.handlers[5] = {x: this.selectionX + this.selectionWidth / 2 - handlerD, y: this.selectionY + this.selectionHeight - handlerD, cursor: 's-resize'};

    this.handlers[6] = {x: this.selectionX - handlerD, y: this.selectionY + this.selectionHeight - handlerD, cursor: 'sw-resize'};

    if (!this.keepAspectRatio)
        this.handlers[7] = {x: -1, y: -1};
    else
        this.handlers[7] = {x: this.selectionX - handlerD, y: this.selectionY + this.selectionHeight / 2 - handlerD, cursor: 'w-resize'};

    this.handlers[8] = {x: this.selectionX + this.handlerWidth, y: this.selectionY + this.handlerWidth, w: this.selectionWidth - this.handlerWidth * 2, h: this.selectionHeight - this.handlerWidth * 2, cursor: 'move'};

    ctx.restore();


    ctx.save();
    ctx.lineWidth = 1;
    ctx.strokeStyle = '#666';
    // ctx.strokeStyle = 'red';
    ctx.beginPath();
    // console.log(this.selectionX);
    ctx.rect(this.selectionX + 0.5, this.selectionY + 0.5, this.selectionWidth, this.selectionHeight);
    ctx.stroke();
    for (var i = 0; i < 8; i++) {
        if (this.handlers[i].x < 0) continue;
        this.handlers[i].w = this.handlerWidth;
        this.handlers[i].h = this.handlerWidth;
        ctx.beginPath();
        ctx.rect(this.handlers[i].x, this.handlers[i].y, this.handlerWidth, this.handlerWidth);
        ctx.stroke();
    }
    ctx.restore();
};

_MIImagePicker.prototype.onFileValueChange = function(evt) {
    console.log(this.file.files);
    if (this.file.length === 0) return;

    var f = this.file.files[0];
    if ( ! MIUtils.isImageFile(f)) {
        if (typeof this.options.onerror === 'function') {
            this.options.onerror.call(this, 1, 'Unsupported image file', f);
        }
        return;
    }
    this.image = new Image();
    var reader = new FileReader();
    var self = this;
    reader.onload = function(e) {
        console.log(self);
        self.image.src = e.target.result;
        var _self = self;
        self.image.onload = function(e) {
            _self.onImageReady();
        };
    };
    reader.onprogress = function(e) {
        console.log(e.loaded + "/" + e.total);
    };
    reader.onerror = function(e) {
        console.log('Read file failed');
        console.log(e);
    };
    reader.readAsDataURL(f);
};


_MIImagePicker.prototype.getSelectionAsImageData = function() {
    if (! this.image) return null;
    var cw = + this.canvas.getAttribute('width');
    var ch = + this.canvas.getAttribute('height');
    var iw = this.image.width;
    var ih = this.image.height;
    var factor = 1.0;
    if (iw > cw || ih > ch) factor = Math.max(iw / cw, ih / ch);
    var dx = (this.selectionX - this.selectionMinX) * factor;
    var dy = (this.selectionY - this.selectionMinY) * factor;
    var dw = this.selectionWidth * factor;
    var dh = this.selectionHeight * factor;
    var tempCanvas = document.createElement('canvas');
    tempCanvas.width = dw;
    tempCanvas.height = dh;
    tempCanvas.style.width = dw + 'px';
    tempCanvas.style.height = dh + 'px';
    // var box = document.createElement('div');
    // box.style.width = '0px';
    // box.style.height = '0px';
    // box.style.overflow = 'hidden';
    // box.appendChild(tempCanvas);
    // document.body.appendChild(box);
    tempCanvas.getContext('2d').drawImage(this.image, dx, dy, dw, dh, 0, 0, dw, dh);
    var retData = tempCanvas.toDataURL('image/jpeg');
    // document.body.removeChild(box);
    return retData;
};

var MIUtils = {
    /**
     * 检测所选择的文件是否一个有效的图片。
     * 检测文件的 type 是否是 image/jpe, image/jpeg, image/png
     * @param Object 在 HTML5 中的 input[type='file'] 选择了文件之后的文件对象
     * @return boolean 如果是一个图片文件，则返回 true，否则返回 false
     */
    isImageFile: function(file) {
        if ( ! file) return false;
        if ( ! file.type) {
            console.log('Can not get the file.type property ');
            console.log(file);
            return false;
        }

        if (file.type === 'image/jpg'
            || file.type === 'image/jpeg'
            || file.type === 'image/png') {
            return true;
        }
        console.log(file.name + ' : ' + file.type);
        return false;
    },

    /**
     * 将 Canvas 中的 base64 编码的图片数据转换成 BLOB 对象。
     * @param String base64 编码的图片数据
     * @return BLOB 对象
     */
    dataURItoBlob: function(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type:mimeString});
    }
};
