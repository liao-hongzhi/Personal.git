package java6326;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
public class Score {
	public static void main(String[] args) throws IOException{
		//1、导入html文件，保存成Document文件
		Document file = Jsoup.parse(new File("C:\\Users\\86183\\eclipse-workspace\\1lesson6326\\src\\small.html"),"UTF-8");
		Document file1 = Jsoup.parse(new File("C:\\Users\\86183\\eclipse-workspace\\1lesson6326\\src\\all.html"),"UTF-8");
		Elements exps = file.getElementsByClass("interaction-rows");
		exps.addAll(file1.getElementsByClass("interaction-row"));
		Properties all = new Properties();
		all.load(new FileInputStream("C:\\Users\\86183\\eclipse-workspace\\1lesson6326\\src\\total.properties"));
		
		double examSc = 0;
		double seExSc = 0;
		double baseSc = 0;
		double proSc = 0;
		double plusSc = 0;
		
		//通过data-appraise-type="TEACHER"（只有老师评）
		//来筛选出"编程题"、"附加题"
		//再通过是否包含"编程"、"附加"，来分辨两者
		Elements proPlus = exps.select("[data-appraise-type=TEACHER]");
		for(Element p:proPlus) {
			String type = p.text();
			if(type.contains("编程")) {
				proSc += getSc(p);
			}
			else if(type.contains("附加")) {
				plusSc += getSc(p);
			}
		}
		
		//通过data-type="QUIZ"（问答题）
		//来筛选出"自测"和"课堂提问"
		//再通过是否包含"自测"来分辨两者
		Elements seExam = exps.select("[data-type=QUIZ]");
		for(Element s:seExam) {
			String type = s.text();
			if(type.contains("自测")) {
				seExSc += getSc(s);
			}
		}
		
		//通过data-appraise-type="APPRAISER"（助教、老师评）、data-appraise-type="EACH_OTHER"（互评）
		//来筛选出"课堂完成部分"、"小测"
		//再通过是否包含"完成"、"小测"，来分辨两者
		Elements baExam1 = exps.select("[data-appraise-type=APPRAISER]");
		for(Element b1:baExam1) {
			String type = b1.text();
			if(type.contains("完成")) {
				baseSc += getSc(b1);
			}
			else if(type.contains("小测")) {
				examSc += getSc(b1);
			}
		}
		Elements baExam2 = exps.select("[data-appraise-type=EACH_OTHER]");
		for(Element b2:baExam2) {
			String type = b2.text();
			if(type.contains("完成")) {
				baseSc += getSc(b2);
			}
			else if(type.contains("小测")) {
				examSc += getSc(b2);
			}
		}
		
		//通过要求的公式进行计算总分
		examSc = (examSc/Double.parseDouble(all.getProperty("test")))*100*0.2;
		seExSc = (seExSc/Double.parseDouble(all.getProperty("before")))*100*0.25;
		baseSc = (baseSc/Double.parseDouble(all.getProperty("base")))*100*0.3*0.95;
		proSc = (proSc/Double.parseDouble(all.getProperty("program")))*95*0.1;
		plusSc = (plusSc/Double.parseDouble(all.getProperty("add")))*90*0.05;
		double allSc = examSc+seExSc+baseSc+proSc+plusSc;
		System.out.printf("%.2f", allSc);
	}

	private static double getSc(Element row) {
		//通过正则表达式对div块内容进行筛选，获得经验值
		double sc = 0;
        String score = row.text();
        Pattern number = Pattern.compile("(\\d+) 经验");
        Matcher matcher = number.matcher(score);
        while (matcher.find()) {
            sc += Double.parseDouble(matcher.group(1));
        }
		return sc;
	}
}
